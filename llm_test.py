import trafilatura
import sys

url = sys.argv[1]

content = trafilatura.fetch_url(url)

output = trafilatura.extract(content, include_tables=False, no_fallback=True,
include_formatting=True)

# print(output)
